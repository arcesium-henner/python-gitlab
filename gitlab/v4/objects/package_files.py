from gitlab.base import RESTManager, RESTObject
from gitlab.mixins import ListMixin


__all__ = ["ProjectPackageFile", "ProjectPackageFileManager"]


class ProjectPackageFile(RESTObject):
    pass


class ProjectPackageFileManager(ListMixin, RESTManager):
    _path = "/projects/%(project_id)s/packages/%(package_id)s/package_files"
    _obj_cls = ProjectPackageFile
    _from_parent_attrs = {"project_id": "project_id", "package_id": "id"}
